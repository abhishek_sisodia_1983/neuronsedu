using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using neuronsEdu.Model;

namespace neuronsEdu.Pages
{
    public class univercityDetailModel : PageModel
    {
        private readonly ApplicationDbContext _db;
        public univercityDetailModel(ApplicationDbContext db)
        {
            _db = db;
        }

        [BindProperty]
        public Univercity univercity { get; set; }

        public async Task OnGet(String univercityCode)
        {
            univercity = String.IsNullOrEmpty(univercityCode) ? null : await _db.Univercities.Where(x => x.UnivercityCode == univercityCode).FirstOrDefaultAsync();
        }
    }
}
