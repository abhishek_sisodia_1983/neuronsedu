using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using neuronsEdu.Model;

namespace neuronsEdu.Pages
{
    public class newsroomModel : PageModel
    {
        private readonly ApplicationDbContext _db;
        public newsroomModel(ApplicationDbContext db)
        {
            _db = db;
        }
        public IEnumerable<NewsManagement> NewsManagements { get; set; }

        [BindProperty]
        public NewsManagement newsmanagement { get; set; }
        public async Task OnGetAsync()
        { 
            NewsManagements = await _db.News.ToListAsync();
        }
    }
}
