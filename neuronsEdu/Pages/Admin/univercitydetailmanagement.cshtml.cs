using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using neuronsEdu.Model;

namespace neuronsEdu.Pages.Admin
{
    public class univercitydetailmanagementModel : PageModel
    {
        private readonly ApplicationDbContext _db;
        public univercitydetailmanagementModel(ApplicationDbContext db)
        {
            _db = db;
        }

        public IEnumerable<Univercity> univercities { get; set; }

        [BindProperty]
        public Univercity univercity { get; set; }

        public async Task OnGet(String univercityCode)
        {
            univercities = await _db.Univercities.ToListAsync();
            univercity = String.IsNullOrEmpty(univercityCode) ? null: await _db.Univercities.Where(x => x.UnivercityCode == univercityCode).FirstOrDefaultAsync();
        }


        public async Task<IActionResult> OnPost()
        {
            if (ModelState.IsValid)
            {
                if (univercity.UnivercityId == null)
                {
                    univercity.SideHtml = univercity.SideHtml == null ? "" : univercity.SideHtml.Replace("&nbsp;", "").Replace("<br />", "");
                    univercity.DetailHtml = univercity.DetailHtml == null ? "" : univercity.DetailHtml.Replace("&nbsp;", "").Replace("<br />", "");
                    univercity.sliderImagesHtml = univercity.sliderImagesHtml == null ? "" : univercity.sliderImagesHtml.Replace("&nbsp;", "").Replace("<br />", "");

                    await _db.Univercities.AddAsync(univercity);
                    await _db.SaveChangesAsync();
                }
                else
                {
                    var univercityToUpdate = await _db.Univercities.FindAsync(univercity.UnivercityId);
                    univercityToUpdate.UnivercityCode = univercity.UnivercityCode;
                    univercityToUpdate.Name = univercity.Name;
                    univercityToUpdate.Country = univercity.Country;
                    univercityToUpdate.SideHtml = univercity.SideHtml == null ? "" : univercity.SideHtml.Replace("&nbsp;", "").Replace("<br />", "");
                    univercityToUpdate.DetailHtml = univercity.DetailHtml == null ? "" : univercity.DetailHtml.Replace("&nbsp;", "").Replace("<br />", "");
                    univercityToUpdate.sliderImagesHtml = univercity.sliderImagesHtml == null ? "" : univercity.sliderImagesHtml.Replace("&nbsp;", "").Replace("<br />", "");
                    
                    await _db.SaveChangesAsync();
                }

                return RedirectToPage("UnivercityDetailList");
            }
            else
            {
                return Page();
            }
        }
    }
}
