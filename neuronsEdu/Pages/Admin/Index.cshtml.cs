using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using neuronsEdu.Model;

namespace neuronsEdu.Pages.Admin
{
    
    public class Index1Model : PageModel
    {
        public readonly ApplicationDbContext _db;

        public Index1Model(ApplicationDbContext db)
        {
            _db = db;
        }
        public IEnumerable<UserLogin> UserLogins { get; set; }
        [BindProperty]
        public UserLogin userlogins { get; set; }
        public IActionResult OnGet()
        {


            if (User.Identity.IsAuthenticated)
            {
                return RedirectToPage("/Admin/Index");
            }
            else
            {

                return Page();
            }
        }
        public async Task<IActionResult> OnPostAsync(string returnUrl = null)
        {
            // returnUrl = returnUrl ?? Url.Content("~/");

            if (ModelState.IsValid)
            {
                var result = await _db.Users.Where(ex => ex.User_email == userlogins.User_email && ex.User_password == userlogins.User_password).ToListAsync();
                if (result.Count > 0)
                {
                    Response.Cookies.Append("abc", (result.FirstOrDefault().User_id).ToString());
                    return RedirectToPage("Dashboard");
                }
                else
                {
                    return Page();
                }
            } 
            return Page();
        }

    }
}
