using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using neuronsEdu.Model;

namespace neuronsEdu.Pages.Admin
{
    public class UnivercityDetailListModel : PageModel
    {
        private readonly ApplicationDbContext _db;
        public UnivercityDetailListModel(ApplicationDbContext db)
        {
            _db = db;
        }
        public IEnumerable<Univercity> univercities { get; set; }

        [BindProperty]
        public Univercity univercity { get; set; }

        public async Task OnGet()
        {
            univercities = await _db.Univercities.ToListAsync();
        }

        public async Task<IActionResult> OnPostDelete(int id)
        {
            var univercity = await _db.Univercities.FindAsync(id);
            if (univercity == null)
            {
                return NotFound();
            }

            _db.Univercities.Remove(univercity);
            _db.SaveChanges();
            return RedirectToPage("UnivercityDetailList");
        }
    }
}
