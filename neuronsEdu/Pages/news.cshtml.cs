using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using neuronsEdu.Model;

namespace neuronsEdu.Pages
{
    public class newsModel : PageModel
    {
         
        private readonly ApplicationDbContext _db;
        public newsModel(ApplicationDbContext db)
        {
            _db = db;
        }
        public IEnumerable<NewsManagement> NewsManagements { get; set; }

        [BindProperty]
        public NewsManagement newsmanagement { get; set; }
        public async Task OnGetAsync(int? id)
        {
            newsmanagement = (id != null && id != 0) ? await _db.News.FindAsync(id) : null;
        }

    }
    
}
