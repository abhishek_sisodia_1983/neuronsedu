﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using neuronsEdu.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace neuronsEdu.Pages
{
	public class IndexModel : PageModel
	{
        private readonly ApplicationDbContext _db;
        public IndexModel(ApplicationDbContext db)
        {
            _db = db;
        }
        public IEnumerable<Notification> notifications { get; set; }
        public IEnumerable<GName> GalleryNames { get; set; } 
       
        [BindProperty]
        public Notification notification { get; set; }
        public GName gallery { get; set; }
        public async Task OnGetAsync()
        {
            notifications = await _db.Notifications.ToListAsync();
            GalleryNames = await _db.GalleryMaster.ToListAsync();
        }
    }
}
