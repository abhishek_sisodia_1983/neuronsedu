﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace neuronsEdu.Model
{
	public class Univercity
	{
		[Key]
		public int? UnivercityId { get; set; }
		public String UnivercityCode { get; set; }
		public String Name { get; set; }
		public String Country { get; set; }
		public String SideHtml { get; set; }
		public String DetailHtml { get; set; }
		public String sliderImagesHtml { get; set; }
	}
}
