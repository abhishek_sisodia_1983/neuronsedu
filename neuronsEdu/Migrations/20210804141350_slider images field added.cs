﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace neuronsEdu.Migrations
{
    public partial class sliderimagesfieldadded : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "sliderImagesHtml",
                table: "Univercities",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "sliderImagesHtml",
                table: "Univercities");
        }
    }
}
