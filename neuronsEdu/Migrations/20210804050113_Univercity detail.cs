﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace neuronsEdu.Migrations
{
    public partial class Univercitydetail : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Univercities",
                columns: table => new
                {
                    UnivercityId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UnivercityCode = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Country = table.Column<string>(nullable: true),
                    SideHtml = table.Column<string>(nullable: true),
                    DetailHtml = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Univercities", x => x.UnivercityId);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Univercities");
        }
    }
}
