#pragma checksum "F:\CurrentProjects\Kalki\Neurons\neuronsedu\neuronsEdu\Pages\nmc_mci.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "8a531a5abe8e14ba0979bb8f4f1c9f722de84359"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(neuronsEdu.Pages.Pages_nmc_mci), @"mvc.1.0.razor-page", @"/Pages/nmc_mci.cshtml")]
namespace neuronsEdu.Pages
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "F:\CurrentProjects\Kalki\Neurons\neuronsedu\neuronsEdu\Pages\_ViewImports.cshtml"
using neuronsEdu;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"8a531a5abe8e14ba0979bb8f4f1c9f722de84359", @"/Pages/nmc_mci.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"d3e179b8f128e952f7ca71c3af12ba989c1cbc1b", @"/Pages/_ViewImports.cshtml")]
    public class Pages_nmc_mci : global::Microsoft.AspNetCore.Mvc.RazorPages.Page
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("mt-4 text-left"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("method", "post", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral(@"<section class=""background-11 "">
    <div class=""container"">
        <div class=""row"">
            <div class=""col-12 mb-4"">
                <div class=""overflow-hidden"" data-zanim-timeline=""{}"" data-zanim-trigger=""scroll"">
                    <h4 data-zanim='{""delay"":0.1}'>NMC or MCI Approved Medical Colleges List | MBBS Admission Abroad</h4>
                </div>
            </div>
            <div class=""col-lg-8"">
                <div class=""row"">
                    <div class=""col-12"">
                        <img class=""radius-tr-secondary radius-tl-secondary"" src=""assets/images/img/mci-nmc-kalki.jpg"" alt=""MBBS Abroad Consultant"">
                    </div>
                    <div class=""col-12"">
                        <div class=""background-white px-3 mt-6 px-0 py-5 px-lg-5 radius-secondary"">
                            <h3>What is NMC or MCI?</h3>
                            <p class=""dropcap"">
                                The Medical Council of India (MCI) or National Medical ");
            WriteLiteral(@"Commission (NMC) is a screening test, also famous as Foreign Medical Graduates Examination (FMGE) is conducted by the National Board of Examinations (NBE) in India for study MBBS in Abroad. It is mandatory for those Indian students who want to study abroad and complete the medical education from MCI or NMC Approved Medical Colleges in abroad.<br />
                                It’s a great opportunity for Indian students to apply for study in abroad and complete an MBBS degree from the MCI or NMC Approved Foreign Medical Colleges in Abroad.
                            </p>
                            <h3>NMC or MCI Screening Test Key Points 2021</h3>
                            <p>
                                MCI or NMC Screening Test is necessary for Indian students and candidates can prepare for the exam with a positive approach and proper plan. Below we have mentioned some NMC or MCI Screening Test key points for the students:
                                <ul>
                             ");
            WriteLiteral(@"       <li>NMC Screening Test is conducted by the National Board of Examinations (NBE) twice a year.</li>
                                    <li>The Medium of Guidance of the exam is English.</li>
                                    <li>The exam includes 300 MCQs divided into 2 sections.</li>
                                    <li>
                                        No negative marking.
                                    </li>
                                    <li>Candidates need to score at least 50% marks in MBBS for the examination.</li>
                                    <li>No limit of attempts for the NMC or MCI Screening Test.</li>
                                </ul>
                            </p>
                            <h3>Why Look NMC or MCI Approved Colleges?</h3>
                            <p>
                                NMC or MCI recognized Foreign medical colleges abroad provide medical education to aspirants at low tuition fees in the English language. The m");
            WriteLiteral(@"edical students who want to study MBBS in abroad should prefer NMC OR NMC or MCI Approved Medical Universities because of the following reasons:
                                <ul>
                                    <li>
                                        <h5>World Recognized Degree:</h5>
                                        <p>NMC or MCI Approved Medical Universities provide a medical degree that is globally recognized.</p>
                                    </li>
                                    <li>
                                        <h5>World-level teaching standards:</h5>
                                        <p>NMC or MCI recognized colleges deliver the best quality medical education to foreign country students. There are lots of countries like Russia, Kyrgyzstan, the Philippines, Ukraine, Georgia, and many more that have high-quality teaching standards with the best infrastructure.</p>
                                    </li>
                                    <li>
   ");
            WriteLiteral(@"                                     <h5>Reasonable fees:</h5>
                                        <p>Medical education in foreign counties for Indian students is available at reasonable fees. The candidates can apply for admission in NMC or MCI Approved Medical Colleges in abroad.</p>
                                    </li>
                                    <li>
                                        <h5>World-class infrastructure:</h5>
                                        <p>Top medical universities in abroad provide world-class infrastructure to International candidates with all basic facilities.</p>
                                    </li>
                                    <li>
                                        <h5>Great opportunities:</h5>
                                        <p>Indian students have lots of opportunities after completing medical education from abroad. Universities provide lots of opportunities while pursuing the MBBS course.</p>
                         ");
            WriteLiteral(@"           </li>
                                    <li>
                                        <h5>English language programs:</h5>
                                        <p>All the NMC OR MCI recognised foreign medical colleges abroad provide medical education in the English language.</p>
                                    </li>
                                </ul>

                            </p>
                            <h3>Advantages of Studying MBBS in NMC Approved Universities?</h3>
                            <p>
                                Generally, Indian students looking to study MBBS Abroad NMC or MCI Approved Colleges because the medical seats in the medical universities in India are less as compare to applicants. Below we have mentioned the study abroad advantages in NMC or MCI Approved Universities:
                                <ul>
                                    <li>No donation fees required for Indian students in NMC or MCI approved medical universities.</li>");
            WriteLiteral(@"
                                    <li>All NMC or MCI approved foreign medical colleges offer the MBBS program at low tuition fees.</li>
                                    <li>The living cost of most of the foreign countries is affordable for candidates.</li>
                                    <li>Chance to study in universities having world-class infrastructure.</li>
                                    <li>Get a chance to learn in the top medical colleges all over the world.</li>
                                    <li>All medical colleges are WHO & NMC or MCI approved colleges.</li>
                                    <li>NMC or MCIapproved medical colleges provide scholarships to foreign country students.</li>
                                    <li>Candidates get more opportunities after completing the program.</li>
                                </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class=""col-lg-4 text-");
            WriteLiteral("center ml-auto mt-5 mt-lg-0\">\r\n                <div class=\"row\">\r\n                    <div class=\"col\">\r\n                        <div class=\"background-facebook p-5 radius-secondary\">\r\n                            ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("form", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "8a531a5abe8e14ba0979bb8f4f1c9f722de8435911321", async() => {
                WriteLiteral(@"
                                <div class=""row align-items-center"">
                                    <div class=""col-6"">
                                        <input class=""form-control""
                                               type=""hidden""
                                               name=""to""
                                               value=""username@domain.extension"" />
                                        <input class=""form-control""
                                               type=""text""
                                               placeholder=""First name""
                                               aria-label=""Text input with dropdown button"" />
                                    </div>
                                    <div class=""col-6"">
                                        <input class=""form-control""
                                               type=""text""
                                               placeholder=""Last name""
                     ");
                WriteLiteral(@"                          aria-label=""Text input with dropdown button"" />
                                    </div>
                                    <div class=""col-12 mt-4"">
                                        <input class=""form-control""
                                               type=""email""
                                               placeholder=""Email Address""
                                               aria-label=""Text input with dropdown button"" />
                                    </div>
                                    <div class=""col-12 mt-4"">
                                        <input class=""form-control""
                                               type=""Password""
                                               placeholder=""Password""
                                               aria-label=""Text input with dropdown button"" />
                                    </div>
                                    <div class=""col-12 mt-4"">
                          ");
                WriteLiteral(@"              <input class=""form-control""
                                               type=""Password""
                                               placeholder=""Confirm Password""
                                               aria-label=""Text input with dropdown button"" />
                                    </div>
                                </div>
                                <div class=""row align-items-center mt-3"">

                                    <div class=""col-md-12 mt-3"">
                                        <button class=""btn btn-primary btn-block"" type=""submit"">
                                            Create Account
                                        </button>
                                    </div>
                                </div>
                            ");
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.Method = (string)__tagHelperAttribute_1.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_1);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</section>  ");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<neuronsEdu.Pages.nmc_mciModel> Html { get; private set; }
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.ViewDataDictionary<neuronsEdu.Pages.nmc_mciModel> ViewData => (global::Microsoft.AspNetCore.Mvc.ViewFeatures.ViewDataDictionary<neuronsEdu.Pages.nmc_mciModel>)PageContext?.ViewData;
        public neuronsEdu.Pages.nmc_mciModel Model => ViewData.Model;
    }
}
#pragma warning restore 1591
