#pragma checksum "F:\CurrentProjects\Kalki\Neurons\neuronsedu\neuronsEdu\Pages\kist_medical_college.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "f5ce2d19926999da3d224edeb08d6813f0071c3e"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(neuronsEdu.Pages.Pages_kist_medical_college), @"mvc.1.0.razor-page", @"/Pages/kist_medical_college.cshtml")]
namespace neuronsEdu.Pages
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "F:\CurrentProjects\Kalki\Neurons\neuronsedu\neuronsEdu\Pages\_ViewImports.cshtml"
using neuronsEdu;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"f5ce2d19926999da3d224edeb08d6813f0071c3e", @"/Pages/kist_medical_college.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"d3e179b8f128e952f7ca71c3af12ba989c1cbc1b", @"/Pages/_ViewImports.cshtml")]
    public class Pages_kist_medical_college : global::Microsoft.AspNetCore.Mvc.RazorPages.Page
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("mt-4 text-left"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("method", "post", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral(@"<section class=""background-11 "">
    <div class=""container"">
        <div class=""row"">
            <div class=""col-12 mb-4"">
                <div class=""overflow-hidden"" data-zanim-timeline=""{}"" data-zanim-trigger=""scroll"">
                    <h4 data-zanim='{""delay"":0.1}'>  KIST Medical College Nepal | MBBS College | MCI Approved</h4>
                </div>
            </div>
            <div class=""col-lg-8"">
                <div class=""row"">
                    <div class=""col-12"">
                        <img class=""radius-tr-secondary radius-tl-secondary"" src=""assets/images/img/kistmedicalcollegenepal_kalki.png"" alt=""MBBS Abroad Consultant"">
                    </div>
                    <div class=""col-12"">
                        <section>
                            <p style=""text-align: justify;"">
                                <font face=""Verdana"" color=""#000000"">
                                    The KIST medical college Nepal is among the best medical college in Nepal. This me");
            WriteLiteral(@"dical college in Nepal offers world-class education to its students and also provides training by professionals who excel in their separate fields.<br>
                                </font>
                            </p>
                            <p style=""text-align: justify;""><font face=""Verdana"" color=""#000000"">The KIST medical college Kathmandu has been recently established in the year 2006 and aims at providing a multi-disciplinary framework to its students, along with the finest technology and some of the greatest minds to offer their expert skills. This MBBS College in Nepal has the goal to make every student of this college be ready to serve everyone they can in the country, even in the remotest areas.</font></p>
                            <p style=""text-align: justify;""><font face=""Verdana"" color=""#000000"">KIST medical college Nepal offers medical education in several clinical subjects such as medicine, pediatrics, dentistry, surgery, etc. the teaching methods used by the teachers of this ");
            WriteLiteral(@"university are unique only to this university and even clears difficult concepts.</font></p>
                            <p style=""text-align: justify;""><font face=""Verdana"" color=""#000000"">This private medical college in Nepal is affiliated to the Tribhuvan University and is also recognized by the medical council of Nepal. The medium of education used to teach <a href=""#""><b>MBBS in Nepal</b></a> medical college is in the English language, which is why so many international students chose to study MBBS in KIST medical college Nepal.</font></p>
                            <h3 style=""text-align: left;""><font face=""Verdana"" color=""#000000""><b>KIST Medical College Quick Glance</b></font></h3>
                            <p style=""text-align: justify; ""><font face=""Verdana"" color=""#000000"">The following mentioned below are the highlighted features at a Quick Glance about the KIST medical college Nepal:</font></p>
                            <table class=""table"" <tbody="""">
   <tbody>
      <tr>
         <td");
            WriteLiteral(@">
            <p><strong>Name of the university:</strong></p>
         </td>
         <td>
            <p>KIST Medical College, Nepal</p>
         </td>
      </tr>
      <tr>
         <td>
            <p><strong>Type:</strong></p>
         </td>
         <td>
            <p>Private</p>
         </td>
      </tr>
      <tr>
         <td>
            <p><strong>Eligibility criteria:</strong></p>
         </td>
         <td>
            <ul>
               <li>
                  <p>An aggregate of 50% in 10+2 or HSE</p>
               </li>
               <li>
                  <p>Must have PCB as the main subject in 10+2</p>
               </li>
            </ul>
         </td>
      </tr>
      <tr>
         <td>
            <p><strong>NEET examination required:</strong></p>
         </td>
         <td>
            <p>Yes, required score in&nbsp;<a href=""#""><strong>NEET exam</strong></a>&nbsp;is required</p>
         </td>
      </tr>
      <tr>
         <td>
          ");
            WriteLiteral(@"  <p><strong>Entrance examination required:</strong></p>
         </td>
         <td>
            <p>No entrance examination required</p>
         </td>
      </tr>
      <tr>
         <td>
            <p><strong>Approved by:</strong></p>
         </td>
         <td>
            <ul>
               <li>
                  <p>Medical council of Nepal</p>
               </li>
               <li>
                  <p>World health organization</p>
               </li>
            </ul>
         </td>
      </tr>
      <tr>
         <td>
            <p><strong>Medium of education:</strong></p>
         </td>
         <td>
            <p>English language for international students</p>
         </td>
      </tr>
      <tr>
         <td>
            <p><strong>Course duration:</strong></p>
         </td>
         <td>
            <p>5 years</p>
         </td>
      </tr>
      <tr>
         <td>
            <p><strong>Affiliated:</strong></p>
         </td>
         <td>
     ");
            WriteLiteral(@"       <p>Tribhuvan university</p>
         </td>
      </tr>
   </tbody>
   </table>
                            <h3 style=""text-align: left;""><font face=""Verdana"" color=""#000000""><b>Why Choose KIST Medical College Nepal?</b></font></h3>
                            <p style=""text-align: justify;""><font face=""Verdana"" color=""#000000"">The KIST Medical College is situated in a peaceful environment in Nepal. One of the reputed medical colleges to study MBBS in Nepal and get globally approved medical degrees. The college has a highly qualified faculty that focuses on catering to an excellent education. It always focuses on delivering world-class education to international students.</font></p>
                            <ul>
                                <li><font face=""Verdana"" color=""#000000"">One of the most reputed colleges for your medical education.</font></li>
                                <li><font face=""Verdana"" color=""#000000"">Best climatic condition for international students.</font></li>
");
            WriteLiteral(@"                                <li><font face=""Verdana"" color=""#000000"">The spacious library has a vast collection of books for students.</font></li>
                                <li><font face=""Verdana"" color=""#000000"">English language programs are available.</font></li>
                                <li><font face=""Verdana"" color=""#000000"">Simple and easy admission process.</font></li>
                                <li><font face=""Verdana"" color=""#000000"">Recognized by NMC or MCI.</font></li>
                                <li><font face=""Verdana"" color=""#000000"">Provide globally approved medical degrees.</font></li>
                                <li><font face=""Verdana"" color=""#000000"">Best accommodation provided to medical students.</font></li>
                                <li><font face=""Verdana"" color=""#000000"">Availability of Indian food, especially for Indian students.</font></li>
                                <li><font face=""Verdana"" color=""#000000"">Listed in top medical colleg");
            WriteLiteral("es in Nepal.</font></li>\r\n                            </ul>\r\n                            <h3><font face=\"Verdana\" color=\"#000000\"><b");
            BeginWriteAttribute("style", " style=\"", 7365, "\"", 7373, 0);
            EndWriteAttribute();
            WriteLiteral(@">KIST Medical College Ranking</b></font></h3>
                            <p style=""text-align: justify;""><font face=""Verdana"" color=""#000000"">According to the KIST medical college, Nepal's world ranking is on the 21789<sup>th</sup> position but is ranked at the 22<sup>nd</sup> position for the best medical university in Nepal.</font></p>
                            <h2><font face=""Verdana"" color=""#000000""><b>KIST Medical College Fee Structure 2021:</b></font></h2>
                            <p style=""text-align: justify;""><font face=""Verdana"" color=""#000000"">The following mentioned below is the KIST medical college Nepal fee structure along with detailed and information such as its tuition fees, hostel, and food cost:</font></p>
                            <h3><strong><font face=""Verdana"" color=""#000000"">KIST Medical College Courses</font></strong></h3>
                            <p style=""text-align: justify;""><font face=""Verdana"" color=""#000000"">One of the best MCI Approved Medical Colleges in Nepal");
            WriteLiteral(@", the college offers academic programs in MBBS and BDS courses. Here the MBBS Course is a 4 years and 6 months program where the BDS program is 4 years and 6 months program. Candidates who wish to pursue MBBS in Nepal then it’s the best choice to study the best courses from the college.</font></p>
                            <font face=""Verdana"" color=""#000000"">
                                <p style=""text-align: justify;""><font face=""Verdana"" color=""#000000"">The KIST Medical College Courses offered are mentioned below that students can study at the college at an affordable tuition fee:</font></p>
                                <table class=""table"">
                                    <tbody>
                                        <tr>
                                            <td colspan=""2"">
                                                <h4 class=""text-center bg-blue"">KIST Medical College Courses</h4>
                                            </td>
                                        ");
            WriteLiteral(@"</tr>
                                        <tr>
                                            <td>
                                                <p>Courses</p>
                                            </td>
                                            <td>
                                                <p>Duration</p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <p>MBBS Program</p>
                                            </td>
                                            <td>
                                                <p>4 Years 6 Months</p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <p>BDS Progr");
            WriteLiteral(@"am</p>
                                            </td>
                                            <td>
                                                <p>4 Years 6 Months</p>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <h2><font face=""Verdana"" color=""#000000""><b>KIST Medical College Eligibility Criteria</b></font></h2>
                                <p><font face=""Verdana"" color=""#000000"">The following mentioned below is the KIST medical college Nepal eligibility criteria to get admission and study MBBS in Nepal:</font></p>
                                <ul>
                                    <li><font face=""Verdana"" color=""#000000"">The age of the applicant should be above 17 years in age</font></li>
                                    <li><font face=""Verdana"" color=""#000000"">The applicant must have completed their 10+2 or ");
            WriteLiteral(@"higher secondary education from a recognized board</font></li>
                                    <li><font face=""Verdana"" color=""#000000"">The applicant must have chemistry, physics, and biology as their main subjects in 10 + 2</font></li>
                                    <li><font face=""Verdana"" color=""#000000"">The applicant must get a min. a score of 50% in a higher secondary examination or 10 + 2</font></li>
                                    <li><font face=""Verdana"" color=""#000000"">The applicant must get a valid score in the NEET entrance examination to get admission and study MBBS in Nepal</font></li>
                                </ul>
                                <h2 style=""text-align: left; ""><font face=""Verdana"" color=""#000000""><strong>Admission Process for KIST Medical College 2021:</strong></font></h2>
                                <p align=""justify""><font face=""Verdana"" color=""#000000"">Plans to study MBBS in abroad? Then you can check the KIST medical college information. Also, Y");
            WriteLiteral("ou can connect with our expert counselors to get more info on MBBS Admission in Nepal by <a href=\"javascript:void(0)\"");
            BeginWriteAttribute("class", " class=\"", 12611, "\"", 12619, 0);
            EndWriteAttribute();
            WriteLiteral(" data-toggle=\"modal\" data-target=\"#myModal2\">clicking here</a>.</font></p>\r\n                                <h3><font face=\"Verdana\" color=\"#000000\"><b");
            BeginWriteAttribute("style", " style=\"", 12771, "\"", 12779, 0);
            EndWriteAttribute();
            WriteLiteral(@">KIST Medical College Documents Required</b></font></h3>
                                <p style=""text-align: justify;""><font face=""Verdana"" color=""#000000"">The following mentioned below is the list of all the documents that are required during the KIST medical college Nepal admission process:</font></p>
                                <ul>
                                    <li><font face=""Verdana"" color=""#000000"">Passport of the applicant which has a minimum validity of 18 months.</font></li>
                                    <li><font face=""Verdana"" color=""#000000"">Certificate of 10<sup>th</sup> and 12<sup>th</sup> mark sheet of the applicant.</font></li>
                                    <li><font face=""Verdana"" color=""#000000"">Birth certificate of the candidate</font></li>
                                    <li><font face=""Verdana"" color=""#000000"">Passport-sized photographs of the applicant</font></li>
                                    <li><font face=""Verdana"" color=""#000000"">Invitation l");
            WriteLiteral(@"etter from the medical college in Nepal.</font></li>
                                    <li><font face=""Verdana"" color=""#000000"">The application documents should be authorized by the ministry of external affairs</font></li>
                                    <li><font face=""Verdana"" color=""#000000"">All documents are too legalized by the Nepali embassy.</font></li>
                                    <li><font face=""Verdana"" color=""#000000"">Medical certificate of the candidate along with the HIV test documents</font></li>
                                </ul>
                                <h3><font face=""Verdana"" color=""#000000""><b");
            BeginWriteAttribute("style", " style=\"", 14451, "\"", 14459, 0);
            EndWriteAttribute();
            WriteLiteral(@">Benefits of Studying in this University</b></font></h3>
                                <p style=""text-align: justify;""><font face=""Verdana"" color=""#000000"">The following mentioned below is the Benefits to study MBBS in KIST medical college Nepal:</font></p>
                                <ul>
                                    <li><font face=""Verdana"" color=""#000000"">No donations are accepted by the KIST medical college Nepal</font></li>
                                    <li><font face=""Verdana"" color=""#000000"">The college is affiliated with Tribhuvan University and offers education to study MBBS in Nepal.</font></li>
                                    <li><font face=""Verdana"" color=""#000000"">The KIST medical college Nepal is recognized by an international organization such as the world health organization</font></li>
                                    <li><font face=""Verdana"" color=""#000000"">The university uses English as their medium of education, which attracts students from all over the worl");
            WriteLiteral(@"d.</font></li>
                                    <li><font face=""Verdana"" color=""#000000"">The KIST medical college Nepal is affiliated with several hospitals and also have its personal teaching college that is used to give in-field training to all the medical students</font></li>
                                    <li><font face=""Verdana"" color=""#000000"">The KIST medical college Nepal is known to offer excellent quality of education</font></li>
                                    <li><font face=""Verdana"" color=""#000000"">The university offers hostel facilities that have fully furnished and modern rooms</font></li>
                                </ul>
                                <h3><font face=""Verdana"" color=""#000000""><b>KIST Medical College Students Life</b></font></h3>
                                <p style=""text-align: justify;""><font face=""Verdana"" color=""#000000"">The following is a list of all the activities and the surrounding environment offered to provide a better experience for all its");
            WriteLiteral(@" students who study MBBS in KIST medical college Nepal:</font></p>
                                <ul>
                                    <li><font face=""Verdana"" color=""#000000"">The college campus offers canteen and mess facilities.</font></li>
                                    <li><font face=""Verdana"" color=""#000000"">The university offers all its students with hostel facility that is fully equitized and have all the modern accommodates</font></li>
                                    <li><font face=""Verdana"" color=""#000000"">The campus of the university has a playground and a gym for all its students</font></li>
                                    <li><font face=""Verdana"" color=""#000000"">The MBBS College in Nepal is an affiliated hospital that is used to provide training to the students; this university has its separate teaching hospital as well where the students are trained by professionals.</font></li>
                                    <li><font face=""Verdana"" color=""#000000"">The campus is surr");
            WriteLiteral(@"ounded by restaurants that provide great food</font></li>
                                </ul>
                                <h3 style=""text-align: left; ""><font face=""Verdana"" color=""#000000""><b>KIST Medical College Hostel and Accommodation</b></font></h3>
                                <p style=""text-align: justify;""><font face=""Verdana"" color=""#000000"">The following is the Hostel and Accommodation offered by the KIST medical college Kathmandu is as following:</font></p>
                                <ul>
                                    <li><font face=""Verdana"" color=""#000000"">Have separate hostels for male and females.</font></li>
                                    <li><font face=""Verdana"" color=""#000000"">The hostels are provided with fully furnished hostel rooms</font></li>
                                    <li><font face=""Verdana"" color=""#000000"">There are options for Air-conditioned rooms</font></li>
                                    <li><font face=""Verdana"" color=""#000000"">The ho");
            WriteLiteral(@"stel of Nepal medical college have mess facilities for its students</font></li>
                                    <li><font face=""Verdana"" color=""#000000"">The hostels are fully protecting and have a good security system</font></li>
                                    <li><font face=""Verdana"" color=""#000000"">The hostels of the KIST medical college Nepal are provided with water and electricity to all its students</font></li>
                                </ul>
                                <h3><font face=""Verdana"" color=""#000000""><b>KIST Medical College FAQs</b></font></h3>
                                <p style=""text-align: justify;""><font face=""Verdana"" color=""#000000"">The following mentioned below are some of the frequently asked questions regarding study MBBS in Nepal in the KIST medical college Nepal:</font></p>
                                <p><font face=""Verdana"" color=""#000000""><b>Que</b>. <b>What is the academic year for KIST medical college Nepal?</b></font></p>
                      ");
            WriteLiteral(@"          <p style=""text-align: justify;""><font face=""Verdana"" color=""#000000""><b>Ans</b>. The academic year for KIST medical college Nepal is from august to September.</font></p>
                                <p style=""text-align: justify;""><font face=""Verdana"" color=""#000000""><b>Que. Is the KIST medical college Nepal recognized by any organization?</b></font></p>
                                <p style=""text-align: justify;""><font face=""Verdana"" color=""#000000""><b>Ans</b>. The KIST medical college Nepal is recognized by several international organizations such as the medical council of Nepal and the world health organization.</font></p>
                                <p style=""text-align: justify;""><font face=""Verdana"" color=""#000000""><b>Que. What is the duration of the degree course of MBBS in KIST medical college Nepal?</b></font></p>
                                <p style=""text-align: justify;""><font face=""Verdana"" color=""#000000""><b>Ans</b>. The degree course for MBBS in KIST medical college N");
            WriteLiteral(@"epal is for a duration period of 5 years.</font></p>
                                <p style=""text-align: justify;""><font face=""Verdana"" color=""#000000""><b>Que. What is the KIST medical college Nepal ranking in Nepal?</b></font></p>
                                <p style=""text-align: justify;""><font face=""Verdana"" color=""#000000""><b>Ans</b>. The Kathmandu medical college ranking is in the 22<sup>nd</sup> position in the country.</font></p>
                                <p style=""text-align: justify;""><font face=""Verdana"" color=""#000000""><b>Que. What are KIST medical college Nepal's eligibility criteria?</b></font></p>
                                <p style=""text-align: justify;""><font face=""Verdana"" color=""#000000""><b>Ans.</b> The KIST medical college Nepal eligibility criteria to get admission requires the candidate to be older than 17 years in age and to have completed their higher secondary education or 10+2 from a recognized board, with PCB as their main subject and score a min. an aggregate of");
            WriteLiteral(@" 50% of its examination. The candidate must also have a valid score in the NEET entrance examination.</font></p>
                            </font>
                        </section>
                    </div>
                </div>
            </div>
            <div class=""col-lg-4 text-center ml-auto mt-5 mt-lg-0"">
                <div class=""row"">
                    <div class=""col"">
                        <div class=""background-facebook p-5 radius-secondary"">
                            ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("form", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "f5ce2d19926999da3d224edeb08d6813f0071c3e27430", async() => {
                WriteLiteral(@"
                                <div class=""row align-items-center"">
                                    <div class=""col-6"">
                                        <input class=""form-control""
                                               type=""hidden""
                                               name=""to""
                                               value=""username@domain.extension"" />
                                        <input class=""form-control""
                                               type=""text""
                                               placeholder=""First name""
                                               aria-label=""Text input with dropdown button"" />
                                    </div>
                                    <div class=""col-6"">
                                        <input class=""form-control""
                                               type=""text""
                                               placeholder=""Last name""
                     ");
                WriteLiteral(@"                          aria-label=""Text input with dropdown button"" />
                                    </div>
                                    <div class=""col-12 mt-4"">
                                        <input class=""form-control""
                                               type=""email""
                                               placeholder=""Email Address""
                                               aria-label=""Text input with dropdown button"" />
                                    </div>
                                    <div class=""col-12 mt-4"">
                                        <input class=""form-control""
                                               type=""Password""
                                               placeholder=""Password""
                                               aria-label=""Text input with dropdown button"" />
                                    </div>
                                    <div class=""col-12 mt-4"">
                          ");
                WriteLiteral(@"              <input class=""form-control""
                                               type=""Password""
                                               placeholder=""Confirm Password""
                                               aria-label=""Text input with dropdown button"" />
                                    </div>
                                </div>
                                <div class=""row align-items-center mt-3"">

                                    <div class=""col-md-12 mt-3"">
                                        <button class=""btn btn-primary btn-block"" type=""submit"">
                                            Create Account
                                        </button>
                                    </div>
                                </div>
                            ");
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.Method = (string)__tagHelperAttribute_1.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_1);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</section>");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<neuronsEdu.Pages.kist_medical_collegeModel> Html { get; private set; }
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.ViewDataDictionary<neuronsEdu.Pages.kist_medical_collegeModel> ViewData => (global::Microsoft.AspNetCore.Mvc.ViewFeatures.ViewDataDictionary<neuronsEdu.Pages.kist_medical_collegeModel>)PageContext?.ViewData;
        public neuronsEdu.Pages.kist_medical_collegeModel Model => ViewData.Model;
    }
}
#pragma warning restore 1591
